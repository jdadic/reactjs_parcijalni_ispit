import React, { Component } from "react";
import "./App.css";
import Search from "./Components/Search";
import Results from "./Components/Results";

export default class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      results: {},
      repos: [],
    };
  }

  handleResult = (dataUser, dataRepo) => {
    this.setState({ results: dataUser, repos: dataRepo });
  };

  resetApp = () => {
    this.setState({ results: "", repos: "" });
  };

  render() {
    return (
      <div className="App">
        <Search onResult={this.handleResult} />
        <Results dataUser={this.state.results} dataRepo={this.state.repos} />
        <br />
        <button onClick={this.resetApp}>Reset</button>
      </div>
    );
  }
}
